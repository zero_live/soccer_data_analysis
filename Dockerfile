FROM python:3.7.0

WORKDIR /soccer
ADD . /soccer

RUN apt update

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
