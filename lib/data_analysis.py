from pandas.plotting import parallel_coordinates
from sklearn.preprocessing import scale
from itertools import cycle, islice
from sklearn.cluster import KMeans
import matplotlib.pyplot as plot
import pandas as pandas
import numpy as numpy
import sqlite3

class DataAnalysis():
  __OVERALL_RATING_COLUMNS = ['potential',  'crossing', 'finishing', 'heading_accuracy','short_passing', 'volleys', 'dribbling', 'curve', 'free_kick_accuracy','long_passing', 'ball_control', 'acceleration', 'sprint_speed','agility', 'reactions', 'balance', 'shot_power', 'jumping', 'stamina','strength', 'long_shots', 'aggression', 'interceptions', 'positioning','vision', 'penalties', 'marking', 'standing_tackle', 'sliding_tackle','gk_diving', 'gk_handling', 'gk_kicking', 'gk_positioning','gk_reflexes']
  __FEATURES = ['gk_kicking', 'potential', 'marking', 'interceptions', 'standing_tackle']
  __SQL_QUERY = "SELECT * FROM Player_Attributes"
  __PREDICTION_FEATURE = 'prediction'
  __OVERALL_RATING_TITLE = 'Player\'s Overall Rating'
  __OVERALL_FILE = "players_overall_rating.png"
  __PARALLEL_FILE = "clusters_in_parallel.png"
  __DATABASE_FILE = 'data/database.sqlite'
  __COLORS = ['b', 'r', 'g', 'y', 'k']
  __PARALLEL_PLOT_SIZE = (15,8)
  __AXES_LIMITS = [-2.5,+2.5]
  __OUTPUT_FOLDER = 'output/'
  __NUMBER_OF_CLUSTERS = 4
  __MARK = 'o'

  def start(self):
    raw_data = self.__retrieve_data_from_database()
    data_frame = self.__remove_nulls_from(raw_data)

    self.__draw_players_overall_rating_graph_for(data_frame)
    self.__draw_clustering_parallel_plots_for(data_frame)


  def __retrieve_data_from_database(self):
    connection = sqlite3.connect(self.__DATABASE_FILE)
    data = pandas.read_sql_query(self.__SQL_QUERY, connection)

    return data

  def __remove_nulls_from(self, data):
    data_without_nulls = data.dropna()

    return data_without_nulls

  def __draw_players_overall_rating_graph_for(self, data_frame):
    correlations = self.__calculate_overall_rating_correlations_with(data_frame)

    overall_rating_data_frame = self.__build_overall_rating_data_frame_from(correlations)
    self.__draw_plot_with(overall_rating_data_frame)

  def __calculate_overall_rating_correlations_with(self, data_frame):
    overall_rating = data_frame['overall_rating']
    correlations = []

    for column in self.__OVERALL_RATING_COLUMNS:
      column_value = data_frame[column]

      correlation = overall_rating.corr(column_value)
      correlations.append(correlation)

    return correlations

  def __build_overall_rating_data_frame_from(self, correlations):
    data_frame = pandas.DataFrame({
      'attributes': self.__OVERALL_RATING_COLUMNS,
      'correlation': correlations
    })

    return data_frame

  def __draw_plot_with(self, data_frame):
    graph = self.__prepare_graph()

    #How hell this know what plot has to fill??????
    self.__fill_axis_with(data_frame)

    graph.savefig(self.__OUTPUT_FOLDER + self.__OVERALL_FILE)

  def __prepare_graph(self):
    figure = plot.gcf()
    figure.set_size_inches(20, 12)
    plot.ylabel(self.__OVERALL_RATING_TITLE)

    return plot

  def __fill_axis_with(self, data_frame):
    axis = data_frame.correlation.plot(linewidth=3.3)

    axis.set_xticks(data_frame.index)
    axis.set_xticklabels(data_frame.attributes, rotation=75)

  def __draw_clustering_parallel_plots_for(self, data_frame):
    data_frame_select = data_frame[self.__FEATURES]
    data_frame_scaled = self.__scale(data_frame_select)

    model = self.__build_model_from(data_frame_scaled)
    clustes_centers = model.cluster_centers_

    graph = self.__pandas_centers(clustes_centers)
    self.__parallel_plot(graph)

  def __scale(self, data_frame):
    copy = data_frame.copy(deep=True)

    scaled = scale(copy)

    return scaled

  def __build_model_from(self, data_frame):
    model = KMeans(n_clusters = self.__NUMBER_OF_CLUSTERS)

    model.fit(data_frame)

    return model

  def __pandas_centers(self, centers):
    names_of_the_columns = list(self.__FEATURES)
    names_of_the_columns.append(self.__PREDICTION_FEATURE)

    zipped_by_prediction = self.__zip_by_prediction(centers)

    graph = pandas.DataFrame(
      zipped_by_prediction,
      columns=names_of_the_columns
    )

    graph['prediction'] = graph['prediction'].astype(int)

    return graph

  def __zip_by_prediction(self, centers):
    #I coulnd't do this work in another way
    zip = [numpy.append(A, index) for index, A in enumerate(centers)]

    return zip

  def __parallel_plot(self, graph):
    colors = self.__prepare_colors_for(graph)

    self.__prepare_plot()

    #How hell this know what plot has to fill??????
    parallel_coordinates(
      graph,
      self.__PREDICTION_FEATURE,
      color=colors,
      marker=self.__MARK
    )

    plot.savefig(self.__OUTPUT_FOLDER + self.__PARALLEL_FILE)

  def __prepare_colors_for(self, graph):
    number_of_clusters = len(graph)

    color_cycle = islice(cycle(self.__COLORS), None, number_of_clusters)

    return list(color_cycle)

  def __prepare_plot(self):
    plot.figure(figsize=self.__PARALLEL_PLOT_SIZE).gca().axes.set_ylim(self.__AXES_LIMITS)
