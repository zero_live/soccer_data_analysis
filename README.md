# Soccer Data Analysis

Recreating example from course [edX Python for Data Science](https://courses.edx.org/courses/course-v1:UCSanDiegoX+DSE200x+3T2018/course/)

## System requirements

### with Docker

- `Docker version 18.06.1-ce or higher`
- `docker-compose version 1.22.0 or higher`

### with Python

- `python 3.7.0 or higher`

## Running the Analysis

The script generates two outputs `players_overall_rating.png` and `clusters_in_parallel.png`, the last one is the final visualization of the data.

### with Docker

- `docker-compose up --build`
- `docker-compose run --rm soccer python script.py`

### with Python

- `pip install --upgrade pip`
- `pip install -r requirements.txt`
- `python script.py`

⋅⋅⋅ You can find the example in [Soccer Data Analysis by Jiayi](https://jiayiwangjw.github.io/2017/06/12/SoccerDataAnalysis/)
