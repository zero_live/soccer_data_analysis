from lib.data_analysis import DataAnalysis

data_analysis = DataAnalysis()

print('Starting Soccer Data Analysis')
data_analysis.start()
print('Data Analysis done, You can find your graphs in output folder')
